
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>

#include "Bruggen.h"

using namespace std;

Bruggen::Bruggen(const char* fileName) {
	ifstream file(fileName);

	if (!file.is_open()) {
		throw runtime_error("File not found");
	}

	file >> size;
	bruggen.reserve(size);
	prijzen.reserve(size);

	int price, winkel;

	while (file >> price >> winkel) {
		bruggen.push_back(winkel);
		prijzen.push_back(price);
	}
}

int Bruggen::getSize() {
	return size;
}
	int ret = 0;


std::vector<int> Bruggen::findSolution(vector<int>& d, int * price) {
	vector<int> result;
	*price = 0;
	
	for (int i = 0; i < d.size(); i++) {
		int priceI = d[i];

		auto collisions = findCollisions(i, d);
		
		int matchupPrice;
		auto matchup = findSolution(collisions, &matchupPrice);

		if (priceI > matchupPrice) {
			result.emplace_back(d[i]);
			*price += priceI;
		}
		else {
			result.emplace_back(matchup);
			*price += matchupPrice;
		}
	}

	return result;
}

/*std::vector<pair<int, int>> Bruggen::findSolution(int index, vector<pair<int, int>>& d, int* price) {
	int price = data[0].first;
	int matchupPrice;
	vector<pair<int, int>> collisions = findCollisions(index, d);
	vector<int> matchup = findSolution()

	for (int i : matchup) {
	}
}
*/