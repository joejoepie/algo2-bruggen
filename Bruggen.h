#ifndef __BRUGGEN_H
#define __BRUGGEN_H

#include <vector>
#include <utility>

using namespace std;

class Bruggen {
public:
	Bruggen(const char* fileName);
	int getSize();
private:
	int size;
	std::vector<int> bruggen;
	std::vector<int> prijzen;

	std::vector<int> findSolution(std::vector<int>& d, int* price);
	std::vector<int> findCollisions(int index, vector<int>& d);
};

#endif
