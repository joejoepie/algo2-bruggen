
#include "Bruggen.h"
#include <iostream>

int main(int argc, char** argv) {
	try {
		Bruggen b("simpelBrug.dat");
		int totalPrice = 0;
		for (int i = 0; i < b.getSize(); i++) {
			int price;
			b.findSolution(i, &price);
			totalPrice += price;
		}
	}
	catch (const char* e) {
		std::cout << e;
	}
}